package com.zebra.common.enums;

/**
 * 演示基础操作类型
 *
 * @author zebra
 *
 */
public enum OperationDemo {
	remove, addSave, editSave, resetPwdSave, update, updateAvatar, clean, syncConfig, authDataScopeSave, changeStatus,authUser,cancelAuthUser,cancelAuthUserAll,selectAuthUserAll
}
